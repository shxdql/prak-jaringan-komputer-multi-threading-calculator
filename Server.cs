﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            // Membuat local endpoint untuk socket
            IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 8080);

            // Membuat TCP socket
            Socket listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Binding
                listener.Bind(localEndPoint);

                // Listening
                listener.Listen(10);

                while (true)
                {

                    Console.WriteLine("Waiting connection ... ");

                    // Menerima koneksi dari client
                    Socket clientSocket = listener.Accept();

                    byte[] bytes = new Byte[1024];
                    string data = null;

                    while (true)
                    {

                        int numByte = clientSocket.Receive(bytes);

                        data += Encoding.ASCII.GetString(bytes, 0, numByte);

                        if (data.IndexOf("<EOF>") > -1)
                            break;
                    }

                    Console.WriteLine("Text received -> {0} ", data);
                    byte[] message = Encoding.ASCII.GetBytes("This is Calculator Program");
                    byte[] message2 = Encoding.ASCII.GetBytes("Follow the instruction to use this!");

                    // Mengirim pesan kepada client
                    clientSocket.Send(message);
                    clientSocket.Send(message2);
                // Fungsi Kalkulator
                

                byte[] FN = new Byte[1024];
                byte[] oprt = new Byte[1024];
                byte[] SN = new Byte[1024];
                string dataFN;
                string dataoprt;
                string dataSN;
                int numFN = clientSocket.Receive(FN);
                int numoprt = clientSocket.Receive(oprt);
                int numSN = clientSocket.Receive(SN);
                dataFN = Encoding.ASCII.GetString(FN, 0, numFN);
                dataoprt = Encoding.ASCII.GetString(oprt, 0, numoprt);
                dataSN = Encoding.ASCII.GetString(SN, 0, numSN);
                Console.WriteLine(dataFN + " " + dataoprt + " " + dataSN);
                int operations = 0;
                double firstnumber = Convert.ToDouble(dataFN);
                double secondnumber = Convert.ToDouble(dataSN);
                double result = 0;
                if(dataoprt == "+")
                {
                    operations = 1;
                }
                else if(dataoprt == "-")
                {
                    operations = 2;
                }
                else if(dataoprt == "*")
                {
                    operations = 3;
                }
                else if(dataoprt == "/")
                {
                    operations = 4;
                }
                switch (operations)
                {
                    case 1:
                        result = firstnumber + secondnumber;
                        break;
                    case 2:
                        result = firstnumber - secondnumber;
                        break;
                    case 3:
                        result = firstnumber * secondnumber;
                        break;
                    case 4:
                        result = firstnumber / secondnumber;
                        break;
                }
                Console.WriteLine("The result = " + result);
                byte[] rslt = Encoding.ASCII.GetBytes("The result = " + result);
                clientSocket.Send(rslt);


                    // Menutup client
                    clientSocket.Shutdown(SocketShutdown.Both);
                    clientSocket.Close();
                }

            

        }
    }   
}
