﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
                // Membuat remote endpoint untuk socket
                IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddr = ipHost.AddressList[0];
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 8080);

                // Membuat TCP Socket
                Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            

                    // Menghubungakn socket ke remote endpoint
                    sender.Connect(localEndPoint);
                    Console.WriteLine("Socket connected to -> {0} ", sender.RemoteEndPoint.ToString());

                    // Testing server dengan mengirim kalimat
                    byte[] messageSent = Encoding.ASCII.GetBytes("Test Client<EOF>");
                    int byteSent = sender.Send(messageSent);
                    byte[] messageReceived = new byte[1024];
                    byte[] message2Received = new byte[1024];

                    // Menerima pesan dari server
                    int byteRecv = sender.Receive(messageReceived);
                    int byte2Recv = sender.Receive(message2Received);
                    Console.WriteLine(Encoding.ASCII.GetString(messageReceived, 0, byteRecv));
                    Console.WriteLine(Encoding.ASCII.GetString(message2Received, 0, byte2Recv));

            // Fungsi Kalkulator
                Console.Write("Type First Number = ");
                string stringFN = Console.ReadLine();
                byte[] FN = Encoding.ASCII.GetBytes(stringFN);
            sender.Send(FN);

            Console.Write("Type the Operation = ");
            string stringoprt = Console.ReadLine();
            byte[] oprt = Encoding.ASCII.GetBytes(stringoprt);
            sender.Send(oprt);

                Console.Write("Type Second Number = ");
                string stringSN = Console.ReadLine();
                byte[] SN = Encoding.ASCII.GetBytes(stringSN);
            sender.Send(SN);

            Console.WriteLine(stringFN + " " + stringoprt + " " + stringSN);
            byte[] theresult = new byte[1024];
            int byteresult = sender.Receive(theresult);
            Console.WriteLine(Encoding.ASCII.GetString(theresult, 0, byteresult));
            Console.WriteLine("Press any key to close . . . .");
            Console.ReadKey();
        }
    }
}
